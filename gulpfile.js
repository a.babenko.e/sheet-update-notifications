const gulp = require('gulp');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const webpack = require('webpack-stream');

const path = {
    build: { // The path to send files
        js: './dist',
    },
    src: { // Path to source files
        js: 'src/index.js',
    },
    watch: { // Files to be watched
        js: 'src/index.js',
    },
};

gulp.task('js', () => gulp.src(path.src.js)
    .pipe(webpack({
        target: 'node',
        mode: 'production',
    }))
    .pipe(babel({
        presets: ['@babel/env'],
    }))
    .pipe(uglify())
    .pipe(rename({ basename: 'index' }))
    .pipe(gulp.dest(path.build.js)));

gulp.task('watch', () => {
    gulp.watch(path.watch.js, 'js');
});

gulp.task('default', () => {
    gulp.start('js');
});
