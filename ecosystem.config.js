module.exports = {
    apps: [
        {
            name: 'app',
            script: 'dist/index.js',
            autorestart: true,
        }],
};
