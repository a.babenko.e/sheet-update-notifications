const express = require('express');

const index = express();
require('dotenv').config();
const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const request = require('request');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
const TelegramBot = require('node-telegram-bot-api');

const TOKEN_PATH = 'token.json';
const token = process.env.TOKEN;
const sheetId = process.env.SPREADSHEET_ID;
const updateInterval = 60000 * 15;

const credsRequestOptions = {
    method: 'GET',
    url: process.env.CREDS_URL,
    headers: { 'cache-control': 'no-cache', 'x-apikey': process.env.DB_APIKEY },
};

const tokenRequestOptions = {
    method: 'GET',
    url: process.env.TOKEN_URL,
    headers: { 'cache-control': 'no-cache', 'x-apikey': process.env.DB_APIKEY },
};

let bot;

if (process.env.MODE = 'DEVELOPMENT') {
    bot = new TelegramBot(token, { polling: true });
} else {
    bot = new TelegramBot(
        token,
        { webHook: { port: process.env.PORT || 5000, host: process.env.HOST || '0.0.0.0' } },
    );
    // Webhook initialization
    bot.setWebHook(`${process.env.EXTERNAL_URL}/:${process.env.PORT || 5000}/bot${token}`);
}

const linkToCell = `https://docs.google.com/spreadsheets/d/${sheetId}/edit#gid=0&range=`;

let authData;
let previousSheetData;

const chatIds = process.env.CHAT_IDS ? process.env.CHAT_IDS.split(' ') : [];

bot.on('text', (message) => {
    if (message.text.toLowerCase().indexOf('/start') === 0) {
        bot.sendMessage(message.chat.id, `Give this code to administrator: ${message.chat.id}`);
    }
});

request(credsRequestOptions, (err, res, body) => {
    authorize(JSON.parse(body)[0]);
});

function getSheetData(auth) {
    const sheets = google.sheets({ version: 'v4', auth });
    return sheets.spreadsheets.values.get({
        spreadsheetId: sheetId,
        range: '1:122',
    }, (err, res) => {
        if (err) return console.log(`The API returned an error: ${err}`);
        if (res) {
            checkUpdated(res.data.values);
        }
    });
}

function checkUpdated(data) {
    if (!previousSheetData && data) {
        previousSheetData = data;
        checkUpdated(data);
        return;
    }

    const sheetsData = data;
    const changedCells = [];
    if (previousSheetData) {
        sheetsData.forEach((row, index) => {
            row.forEach((cell, i) => {
                if (previousSheetData[index][i] !== cell) {
                    changedCells.push({
                        oldValue: previousSheetData[index][i],
                        newValue: cell,
                        rowIndex: index + 1,
                        columnIndex: i + 1,
                    });
                }
            });
        });

        if (changedCells.length) {
            const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
            let msg = 'Было изменено: \n';
            let inieted;

            changedCells.forEach((cell) => {
                if (cell.oldValue) {
                    const colLetter = (alphabet[cell.columnIndex - 1]).toUpperCase();
                    msg = `${msg} \n [(${cell.rowIndex}, ${colLetter})](${linkToCell}${colLetter}${cell.rowIndex}) \n *Было:* ${cell.oldValue} \n *Стало:* ${cell.newValue}`;
                    msg = `${msg} \n ———————— \n`;
                    inieted = true;
                }
            });
            if (inieted) {
                chatIds.forEach((id) => {
                    bot.sendMessage(id, msg, { parse_mode: 'markdown' });
                });
            }
        }
    }

    previousSheetData = data;
    setTimeout(() => {
        getSheetData(authData);
    }, updateInterval);
}

function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error while trying to retrieve access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0],
    );

    request(tokenRequestOptions, (err, res, body) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(body)[0]);
        authData = oAuth2Client;
        getSheetData(oAuth2Client);
    });
}

module.exports = index;
